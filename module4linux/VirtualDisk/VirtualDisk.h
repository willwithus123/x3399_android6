#ifndef __VIRTUALDISK__
#define __VIRTUALDISK__
#include <linux/module.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/init.h>
#include <linux/cdev.h>
#include <asm/io.h>
#include <linux/slab.h>

#define VIRTUALDISK_SIZE 0x2000
#define MEM_CLEAR 0x1
#define PORT1_SET 0x2
#define PORT2_SET 0x3
#define VIRTUALDISK_MAJOR 200

static int VirtualDisk_major = VIRTUALDISK_MAJOR;

struct VirtualDisk
{
    struct cdev cdev;
    unsigned char mem[VIRTUALDISK_SIZE];
    int port1;
    long port2;
    long counter;
};
#endif
