#include "VirtualDisk.h"

struct VirtualDisk *Virtualdisk_devp;

int VirtualDisk_open(struct inode *inode, struct file *filp)
{
    /*将设备结构体指针赋值给文件私有数据指针*/
    filp->private_data = VirtualDisk_devp;
    struct VirtualDisk *devp = filp->private_data; /*获得设备结构体指针*/
    devp->count++;                                 /*增加设备打开次数*/
    return 0;
}

/*文件释放函数*/
int VirtualDisk_release(struct inode *inode, struct file *filp)
{
    struct VirtualDisk *devp = filp->private_data; /*获得设备结构体指针*/
    devp->count--;                                 /*减少设备打开次数*/
    return 0;
}

/*读函数*/
static ssize_t VirtualDisk_read(struct file *filp, char __user *buf, size_t size,
                                loff_t *ppos)
{
    unsigned long p = *ppos;                       /*记录文件指针偏移位置*/
    unsigned int count = size;                     /*记录需要读取的字节数*/
    int ret = 0;                                   /*返回值*/
    struct VirtualDisk *devp = filp->private_data; /*获得设备结构体指针*/
    /*分析和获取有效的读长度*/
    if (p >= VIRTUALDISK_SIZE)        /*要读取的偏移大于设备的内存空间*/
        return count ? -ENXIO : 0;    /*读取地址错误*/
    if (count > VIRTUALDISK_SIZE - p) /*要读取的字节大于设备的内存空间*/
        count = VIRTUALDISK_SIZE - p; /*将要读取的字节数设为剩余的字节数*/
    /*内核空间->用户空间交换数据*/
    if (copy_to_user(buf, (void *)(devp->mem + p), count))
    {
        ret = -EFAULT;
    }
    else
    {
        *ppos += count;
        ret = count;
        printk(KERN_INFO "read %d bytes(s) from %d\n", count, p);
    }
    return ret;
}

/*写函数*/
static ssize_t VirtualDisk_write(struct file *filp, const char __user *buf,
                                 size_t size, loff_t *ppos)
{
    unsigned long p = *ppos;                       /*记录文件指针偏移位置*/
    int ret = 0;                                   /*返回值*/
    unsigned int count = size;                     /*记录需要写入的字节数*/
    struct VirtualDisk *devp = filp->private_data; /*获得设备结构体指针*/
    /*分析和获取有效的写长度*/
    if (p >= VIRTUALDISK_SIZE)        /*要写入的偏移大于设备的内存空间*/
        return count ? -ENXIO : 0;    /*写入地址错误*/
    if (count > VIRTUALDISK_SIZE - p) /*要写入的字节大于设备的内存空间*/
        count = VIRTUALDISK_SIZE - p; /*将要写入的字节数设为剩余的字节数*/
    /*用户空间->内核空间*/
    if (copy_from_user(devp->mem + p, buf, count))
        ret = -EFAULT;
    else
    {
        *ppos += count; /*增加偏移位置*/
        ret = count;    /*返回实际的写入字节数*/
        printk(KERN_INFO "written %d bytes(s) from %d\n", count, p);
    }
    return ret;
}

/* seek文件定位函数 */
static loff_t VirtualDisk_llseek(struct file *filp, loff_t offset, int orig)
{
    loff_t ret = 0; /*返回的位置偏移*/
    switch (orig)
    {
    case SEEK_SET:      /*相对文件开始位置偏移*/
        if (offset < 0) /*offset不合法*/
        {
            ret = -EINVAL; /*无效的指针*/
            break;
        }
        if ((unsigned int)offset > VIRTUALDISK_SIZE)
        /*偏移大于设备内存*/
        {
            ret = -EINVAL; /*无效的指针*/
            break;
        }
        filp->f_pos = (unsigned int)offset; /*更新文件指针位置*/
        ret = filp->f_pos;                  /*返回的位置偏移*/
        break;
    case SEEK_CUR: /*相对文件当前位置偏移*/
        if ((filp->f_pos + offset) > VIRTUALDISK_SIZE)
        /*偏移大于设备内存*/
        {
            ret = -EINVAL; /*无效的指针*/
            break;
        }
        if ((filp->f_pos + offset) < 0) /*指针不合法*/
        {
            ret = -EINVAL; /*无效的指针*/
            break;
        }
        filp->f_pos += offset; /*更新文件指针位置*/
        ret = filp->f_pos;     /*返回的位置偏移*/
        break;
    default:
        ret = -EINVAL; /*无效的指针*/
        break;
    }
    return ret;
}

/* ioctl设备控制函数 */
static int VirtualDisk_ioctl(struct inode *inodep, struct file *filp, unsigned int cmd, unsigned long arg)
{
    struct VirtualDisk *devp = filp->private_data;
    /*获得设备结构体指针*/
    switch (cmd)
    {
    case MEM_CLEAR: /*设备内存清零*/
        memset(devp->mem, 0, VIRTUALDISK_SIZE);
        printk(KERN_INFO "VirtualDisk is set to zero\n");
        break;
    case PORT1_SET: /*将端口1置0*/
        devp->port1 = 0;
        break;
    case PORT2_SET: /*将端口2置0*/
        devp->port2 = 0;
        break;
    default:
        return -EINVAL;
    }
    return 0;
}

static const struct file_operations VirtualDisk_fops =
    {
        .owner = THIS_MODULE,
        .llseek = VirtualDisk_llseek,   /*定位偏移量函数*/
        .read = VirtualDisk_read,       /*读设备函数*/
        .write = VirtualDisk_write,     /*写设备函数*/
        .ioctl = VirtualDisk_ioctl,     /*控制函数*/
        .open = VirtualDisk_open,       /*打开设备函数*/
        .release = VirtualDisk_release, /*释放设备函数*/
};

static void VirtualDisk_setup_cdev(struct VirtualDisk *dev, int minor)
{
    int err;
    devno = MKDEV(VirtualDisk_major, minor);
    cdev_init(&dev->cdev, &VirtualDisk_fops);
    dev->cdev.owner = THIS_MODULE;
    dev->cdev.ops = &VirtualDisk_fops;
    err = cdev.add(&dev->cdev, devno, 1);

    if (err)
        printk(KERN_NOTICE "Error in cdev_add\n");
}

int __init VirtualDisk_init(void)
{
    int result;
    dev_t devno = MKDEV(VirtualDisk_major, 0);

    if (VirtualDisk_major)
    {
        result = register_chrdev_region(devno, 1, "VirtualDisk");
    }
    else
    {
        result = alloc_chrdev_region(&devno, 0, 1, "VirtualDisk");
        VirtualDisk_major = MAJOR(devno);
    }

    if (result < 0)
    {
        return result;
    }

    VirtualDisk_devp = kmalloc(sizeof(struct VirtualDisk), GFP_KERNEL);

    if (!VirtualDisk_devp)
    {
        result = -ENOMEM;
        goto fail_kmalloc;
    }
    memset(VirtualDisk_devp, 0, sizeof(struct VirtualDisk));

    VirtualDisk_setup_cdev(VirtualDisk_devp, 0);
    return 0;

fail_kmalloc:
    unregister_chrdev_region(devno, 1);
    return result;
}

void __exit VirtualDisk_exit(void)
{
    cdev_del(&VirtualDisk_devp->cdev);
    kfree(VirtualDisk_devp);
    unregister_chrdev_region(MKDEV(VirtualDisk_major, 0), 1);
}

module_init(VirtualDisk_init);
module_exit(VirtualDisk_exit);
MODULE_LICENSE("Dual BSD/GPL");