#
# Copyright 2014 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

$(call inherit-product, $(LOCAL_PATH)/rk3399_64.mk)

PRODUCT_NAME := rk3399_disvr
PRODUCT_DEVICE := rk3399_disvr
PRODUCT_MODEL := rk3399_disvr

# debug-logs
ifneq ($(TARGET_BUILD_VARIANT),user)
MIXIN_DEBUG_LOGS ?= true
endif

DOUBLE_SCREEN ?= YES
ROTATE_SCREEN ?= rotate_90
DUAL_SCREEN ?= false

# if we want to support 2 vop output, we should disabe afbc funciton
# otherwise, we should enable afbc function to speedup the system
BOARD_USE_AFBC_LAYER ?=false

BOOT_SHUTDOWN_ANIMATION_RINGING ?= true
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/rk3399_64_vr/rk-ovr.ko:/system/lib/modules/rk_ovr.ko

ifeq ($(DUAL_SCREEN),true)
# default config is for sharp 2.89' lcd(1440x1440 x 2).
# if you want to adapt to an another lcd ,please modify following configs:
# 1. sys.xxx.x_w adn sys.xxx.x_h, you should change this base on the lcd resolution,
#    the value is lower than resolution, but the width and height must be in same proportion.
# 2. sys.vr.vsync means display is output from fb0 or fb5, generally set this to 0.
# 3. sys.vr.scan means the scan direction, if display image is tearing, please try value 0.
PRODUCT_PROPERTY_OVERRIDES += \
	ro.sf.hwrotation=0 \
	sys.xxx.x_w=2304 \
	sys.xxx.x_h=1152 \
	sys.vr.panel=1 \
	sys.vr.params.inter=0.055 \
	sys.vr.params.vert=0.037 \
	sys.vr.params.screen=0.031 \
	sys.vr.params.bk1=0.19 \
	sys.vr.params.bk2=0.20 \
	vr.video.direct=true \
	sys.vr.emirror=1 \
	sys.vr.pmirror=1 \
	sys.vr.scan=1 \
	sys.vr.vsync=0
else
PRODUCT_PROPERTY_OVERRIDES += \
	ro.sf.hwrotation=0 \
	ro.orientation.einit=270\
	sys.xxx.x_w=1152 \
	sys.xxx.x_h=2048 \
	persist.display.portrait=true\
	sys.vr.vsync=0 \
	sys.vr.scan=0
endif
