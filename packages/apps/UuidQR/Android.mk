LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)
LOCAL_STATIC_JAVA_LIBRARIES := zxing
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES = zxing:libs/zxing.jar
include $(BUILD_MULTI_PREBUILT)  

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(call all-subdir-java-files)
LOCAL_MODULE_TAGS := eng optional debug
LOCAL_JACK_ENABLED := disabled
LOCAL_PACKAGE_NAME := UuidQR
LOCAL_STATIC_JAVA_LIBRARIES := zxing
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES = zxing:libs/zxing.jar
include $(BUILD_PACKAGE)

include $(call all-makefiles-under, $(LOCAL_PATH))
